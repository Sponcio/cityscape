using JetBrains.Annotations;
using System;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.Events;

public enum GameLayer { Forelayer, Midlayer, Backlayer }
public enum GameState { normalState, ploppingState }

public class GameManager : Singleton<GameManager>
{
    [Header("GENERALS")]
    public GameState gameState;
    public List<Color> layerColors = new List<Color>();
    public List<Transform> layerTransforms = new List<Transform>();

    [Header("PLOPPABLE DATA")]
    public Building buildingToPlop;
    public GameLayer buildingToPlopLayer;
     Vector3 buildingToPlopPosition;

    [Header("SCORE")]
    public int currentScore;


    protected override void Awake()
    {
        base.Awake();
        gameState = GameState.normalState;
    }

    private void Update()
    {
        switch (gameState)
        {
            case GameState.normalState:
                UpdateNormalState();
                break;

            case GameState.ploppingState:
                UpdatePloppingState();
                break;

            default:
                break;
        }
    }

    private void UpdateNormalState()
    {
        
    }

    private void UpdatePloppingState()
    {
        if (Input.GetMouseButtonDown(0))
        {
            buildingToPlop.isPreview = false;
            //buildingToPlop = null; //Serve?
            buildingToPlop.transform.parent = layerTransforms[(int)buildingToPlopLayer];
            gameState = GameState.normalState;
        }

        if (Input.GetMouseButtonDown(1))
        {
            Destroy(buildingToPlop.gameObject);
            gameState = GameState.normalState;
        }   
    }

    public void PloppingBuildingToPlop()
    {
        buildingToPlopPosition = layerTransforms[(int)buildingToPlopLayer].position;
        buildingToPlop = Instantiate(buildingToPlop, buildingToPlopPosition, Quaternion.identity);
    }
}


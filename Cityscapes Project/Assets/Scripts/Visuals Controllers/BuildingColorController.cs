using System.Net.NetworkInformation;
using UnityEngine;

public class BuildingColorController : MonoBehaviour
{
    [SerializeField, Range(0f, 1f)] private float previewOpacity = 0.5f;
    [SerializeField] private SpriteRenderer colorControllerSpriteRenderer;
    [SerializeField] private Building colorControllerBuilding;

    private void Awake()
    {
        colorControllerSpriteRenderer = GetComponent<SpriteRenderer>();
        colorControllerBuilding = GetComponentInParent<Building>();

        SetColorByBuildingLayer();
    }

    private void Update()
    {
        float targetOpacity = (colorControllerBuilding.isPreview) ? previewOpacity : 1f;
        SetOpacityForPreview(targetOpacity);
    }

    private void SetOpacityForPreview(float opacity)
    {
        opacity = Mathf.Clamp01(opacity);
        colorControllerSpriteRenderer.color = new Color(colorControllerSpriteRenderer.color.r, colorControllerSpriteRenderer.color.g, colorControllerSpriteRenderer.color.b, opacity);
    }

    private void SetColorByBuildingLayer()
    {
        int layerIndex = (int)colorControllerBuilding.buildingGameLayer;
        if (layerIndex >= 0 && layerIndex < GameManager.Instance.layerColors.Count)
        {
            colorControllerSpriteRenderer.color = GameManager.Instance.layerColors[layerIndex];
        }
        else
        {
            Debug.LogError("Can't assign color to building");
        }
    }
}

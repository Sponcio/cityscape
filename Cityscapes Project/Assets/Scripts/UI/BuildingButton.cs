using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuildingButton : MonoBehaviour
{
    [SerializeField] Building buildingAssignedToButton;

    public void BuildingButtonClicked()
    {
        GameManager.Instance.buildingToPlop = buildingAssignedToButton;       
        GameManager.Instance.gameState = GameState.ploppingState;
        GameManager.Instance.buildingToPlopLayer = buildingAssignedToButton.buildingGameLayer;
        GameManager.Instance.PloppingBuildingToPlop();
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class LevelGizmos : MonoBehaviour
{
    public Color gizmoColor = Color.white;
    public float lineY = 0f;

    void OnDrawGizmos()
    {
        DrawHorizontalLine();
    }

    void DrawHorizontalLine()
    {
        Gizmos.color = gizmoColor;
        float myZ = transform.position.z;
        Vector3 startPoint = new Vector3(-1000f, lineY, myZ);
        Vector3 endPoint = new Vector3(1000f, lineY, myZ);

        Gizmos.DrawLine(startPoint, endPoint);
    }
}

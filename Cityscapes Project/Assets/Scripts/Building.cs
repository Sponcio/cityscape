using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Building : MonoBehaviour
{
    public string buildingName;
    public GameLayer buildingGameLayer;
    public bool isPreview;

    

    [SerializeField] float buildingRange = 2f;
    [SerializeField] List<Building> nearbyBuildingList = new List<Building>();
    [SerializeField] List<BuildingData> pointsPerBuilding = new List<BuildingData>();


    private void Awake()
    {
        isPreview = true;
        
    }

    private void Update()
    {
        PreviewFollowMouseOnX();
        UpdateNearbyBuildingList();
        AddScorePerNearbyBuilding();
    }

    private void PreviewFollowMouseOnX()
    {
        if (isPreview)
        {
            float buildingX = Camera.main.ScreenToWorldPoint(Input.mousePosition).x;        
            Vector3 newPosition = transform.position;
            newPosition.x = buildingX;
            transform.position = newPosition;
        }

    }

    private void AddScorePerNearbyBuilding() 
    {
        foreach (Building building in nearbyBuildingList)
        {
            foreach (BuildingData BD in pointsPerBuilding)
            {
                if (building.buildingName == BD.name)
                {
                    GameManager.Instance.currentScore += BD.points;
                }
            }
        }
    }

    private void UpdateNearbyBuildingList()
    {
        nearbyBuildingList.Clear();  //Devo apire se questa riga da problemi o meno

        Building[] buildings = FindObjectsOfType<Building>();

        foreach (Building building in buildings)
        {
            if (Mathf.Abs(building.transform.position.x - transform.position.x) <= buildingRange)
            {
                if (!nearbyBuildingList.Contains(building))
                {
                    nearbyBuildingList.Add(building);
                }
            }
            else
            {
                if (nearbyBuildingList.Contains(building))
                {
                    nearbyBuildingList.Remove(building);
                }
            }
        }
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireCube(transform.position, new Vector2(buildingRange*2, 5f));
    }
}

    


